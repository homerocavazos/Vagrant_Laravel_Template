# Set Vagrant API Version
VAGRANTFILE_API_VERSION = "2"
HOSTNAME = "laravel.build"

# Check that the installed Vagrant version is greater than 1.5
if `vagrant --version` < 'Vagrant 1.5.0'
    abort('Your Vagrant is too old. Please install at least 1.5.0')
end

Vagrant.configure(VAGRANTFILE_API_VERSION) do |config|

  # Every Vagrant virtual environment requires a box to build off of.
  config.vm.box = "ubuntu/trusty64"
  config.vm.hostname = HOSTNAME
  config.vm.network :private_network, ip: "192.168.33.70"

  # Virtualbox overrides
  config.vm.provider "virtualbox" do |vb|
    vb.memory = 1024
    vb.cpus = 2
    vb.name = HOSTNAME
    vb.customize ["modifyvm", :id, "--natdnshostresolver1", "on"]
  end

  # Share folder with VM
  config.vm.synced_folder ".", "/vagrant", disabled: true
  config.vm.synced_folder "provisioning/", "/provisioning", owner: 'root', group: 'root', mount_options: ["dmode=777,fmode=664"]
  config.vm.synced_folder "srv/", "/srv", owner: 'www-data', group: 'www-data', mount_options: ["dmode=777,fmode=664"]

   # Fix 'stdin: is not a tty' error that sometimes occurs on Ubuntu with Vagrant
  config.vm.provision "fix-no-tty", type: "shell" do |s|
    s.privileged = false
    s.inline = "sudo sed -i '/tty/!s/mesg n/tty -s \\&\\& mesg n/' /root/.profile"
  end

  # Run the Provisioning Script
  config.vm.provision :shell, :inline => "cd /provisioning && chmod +x initialize.sh && sh initialize.sh"

  # Display Confirmation Message
  config.vm.post_up_message = "\n\nProvisioning is done, visit http://"+HOSTNAME+" to view your sites.\n\n"

end