Laravel on a Vagrant box with Ansible
-------
It is assummed your development machine has VirtualBox, Vagrant and Ansible.


#### Vagrantfile


```sh
# HOSTNAME
Updates: config.vm.hostname and vb.name

# config.vm.network
Update the ip to an available IP address
```

#### Provisioning


```sh
# vars/main.yml
Update 'global_name'
Update 'global_domain and global_domain_aliases' to match the domain name in Vagrantfile.

```

This is all you need to do to get up and running with a local environment. Feel free to adjust for your needs. 